# iReportType
Exemplo de importação (SLA)


FrontEnd (JS/AngularJS (MVC))

Controller:


    // gerar relatorio
    vm.exportarRelatorio = function (tipo) {
        var params = {
            tipo: tipo,
            parametro: vm.model.filtros.parametro,
        };
        CadastroReportService
            .one("gerarRelatorioPDFManterParametroSistema")
            .withHttpConfig({ responseType: 'arraybuffer' })
            .get(params)
            .then(function (res) {
                console.log(res);
                console.log (valor);
                var file = new Blob([res], {type: 'application/vnd.ms-excel'});
                var fileURL = URL.createObjectURL(file);
                var fileURL = URL.createObjectURL(file);
                var FileSaver = require('file-saver');
                FileSaver.saveAs(file, "Relatorio_Manter_Parâmetro_de_Sistema"+"."+tipo);
            });
    };
}


BackEnd (Controller)

	/**
	 * Permite o download do relatorio de Manter Parametro de Sistema a partir do tipo do mesmo
	 * 
	 * @param response
	 * @param tipo
	 * @param session
	 */
	
	@RequestMapping(value = "/gerarRelatorioPDFManterParametroSistema")
	public void gerarRelatorioPDFManterParametroSistema(
			@RequestParam(value = "tipo", required = false, defaultValue = "PDF") String tipo,
			@RequestParam(value = "parametro", required = false, defaultValue = "") String parametro,
																					HttpSession session,
																					HttpServletResponse response)							
		{
		try {
			List<ManterParametroSistema> lista = new ArrayList<ManterParametroSistema>();
			lista = manterParametroSistemaService.listarFiltroValorParametro(parametro);
			
			Map<String, Object> params = new HashMap<>();
	
			OpenReport openReport = new OpenReport();
	
				openReport.gerarRelatorio(PATH_RELATORIO_PARAMETRO_SISTEMA, lista, params, response.getOutputStream(), tipo);
		} catch (Exception e) {
			String mensagemRaiz = ExceptionUtils.getRootCauseMessage(e);
			UtilLog.logger.error("Exceção gerada: ".concat(mensagemRaiz), e);
		}
	}
